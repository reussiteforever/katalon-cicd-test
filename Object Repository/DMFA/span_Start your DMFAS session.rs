<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Start your DMFAS session</name>
   <tag></tag>
   <elementGuidId>96535676-5d41-4878-a4a2-c100c31fa86b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>span.p-button-label</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Password'])[1]/following::span[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>db89e59f-b212-4876-8aa0-c70986e34c28</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>p-button-label</value>
      <webElementGuid>c6c8817f-8d21-4143-b63e-315882b485b9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Start your DMFAS session</value>
      <webElementGuid>da336e9c-e0c9-4fe3-96fd-d078ec8b5ce2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/dmfas-root[1]/div[@class=&quot;theme-dmfas&quot;]/dmfas-login[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;login-body&quot;]/form[@class=&quot;ng-dirty ng-touched ng-valid&quot;]/div[@class=&quot;card login-panel p-fluid&quot;]/div[@class=&quot;grid&quot;]/div[@class=&quot;col-12&quot;]/div[@class=&quot;full-width ng-star-inserted&quot;]/button[@class=&quot;p-button p-component p-element&quot;]/span[@class=&quot;p-button-label&quot;]</value>
      <webElementGuid>6e852aad-2eda-4b91-be4b-e42c883e9408</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Password'])[1]/following::span[2]</value>
      <webElementGuid>68e67185-f2c1-4ef8-8942-17c009abd9ad</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Username'])[1]/following::span[2]</value>
      <webElementGuid>2d34cc89-80a7-4c88-98ed-13747ea0ca70</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Forgot Username'])[1]/preceding::span[1]</value>
      <webElementGuid>e52929dc-f97f-4b27-9515-2e73be6b608e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Forgot Password'])[1]/preceding::span[2]</value>
      <webElementGuid>4f50364c-d41f-4ae7-b175-19739880e01b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Start your DMFAS session']/parent::*</value>
      <webElementGuid>b95ec3f7-6fd8-48b8-bd0f-7e6c4a930988</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span[2]</value>
      <webElementGuid>a5ae6416-7ebd-4c9a-ae50-ea60908619a6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Start your DMFAS session' or . = 'Start your DMFAS session')]</value>
      <webElementGuid>9efca453-0e79-498d-845d-f5a62f149965</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
