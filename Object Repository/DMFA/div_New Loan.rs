<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_New Loan</name>
   <tag></tag>
   <elementGuidId>f256d14d-72d6-480f-8fdd-4ab44ba84ee0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.col-12.screen-title</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Menu Bar'])[1]/following::div[3]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>48c6e513-d362-47e6-8883-fb6602edf512</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-12 screen-title</value>
      <webElementGuid>cdaf04b0-d624-4456-a2cd-8dca1140b45f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>New Loan</value>
      <webElementGuid>48f12165-e5ce-435a-bbc9-d10d14b9a9bd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/dmfas-root[1]/div[@class=&quot;theme-dmfas&quot;]/dmfas-main-layout[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;layout-container&quot;]/div[@class=&quot;content-layout-wrapper&quot;]/dmfas-left-nav-menu[1]/aside[@class=&quot;left-nav-menu&quot;]/div[@class=&quot;col-12 screen-title&quot;]</value>
      <webElementGuid>19a7c466-6953-40d8-a651-32cb1491f65d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Menu Bar'])[1]/following::div[3]</value>
      <webElementGuid>8857d1b2-8c41-4118-acee-97f0a2b91cc4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Calculator'])[1]/following::div[3]</value>
      <webElementGuid>69756063-345b-4d25-abd4-d180efce6cde</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sections'])[1]/preceding::div[1]</value>
      <webElementGuid>85ac56b3-c773-40cd-aeb3-2577930bac31</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='New Loan']/parent::*</value>
      <webElementGuid>45b1ec7a-98bb-4b76-9dbc-10916deb45ec</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//aside/div[2]</value>
      <webElementGuid>befea079-f9d7-48ee-b95f-e69d688d3b19</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'New Loan' or . = 'New Loan')]</value>
      <webElementGuid>7e736e38-b878-492c-9425-fbe157c2de44</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
