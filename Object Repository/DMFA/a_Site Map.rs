<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Site Map</name>
   <tag></tag>
   <elementGuidId>085bb6ef-9153-4483-be95-488123f3bd1e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>a[title=&quot;Switch to Site Map menu layout&quot;]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(),'Site Map')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>a95fc210-de52-49e4-8cba-625323350b17</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Switch to Site Map menu layout</value>
      <webElementGuid>e222fa06-67cf-4c60-991f-30f613127e4f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Site Map </value>
      <webElementGuid>456ebd81-c32f-4d66-83db-d3766e573e02</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/dmfas-root[1]/div[@class=&quot;theme-dmfas&quot;]/dmfas-main-layout[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;layout-container hideButtonBar&quot;]/dmfas-top-menu[1]/nav[@class=&quot;layout-main-menu&quot;]/div[@class=&quot;menu-right&quot;]/ul[@class=&quot;dmfas-menu&quot;]/li[@class=&quot;ng-star-inserted&quot;]/a[1]</value>
      <webElementGuid>90f54da6-37e0-4096-aa30-b302e8490754</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Site Map')]</value>
      <webElementGuid>26fb9bea-87e2-4b10-806b-8d5b385b216a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Calculator'])[1]/following::a[1]</value>
      <webElementGuid>3344f487-8123-482b-a7c1-d0bb2b4a7ca7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Control Panel'])[1]/following::a[2]</value>
      <webElementGuid>d97b0118-1fc6-43b1-bf7d-e5498d2663ec</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Reference Files'])[1]/preceding::a[2]</value>
      <webElementGuid>96c6f049-626b-4e7d-9818-86502448e83f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Participants'])[1]/preceding::a[3]</value>
      <webElementGuid>38e3b7ab-b595-4740-8d90-d4ac44ea6068</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Site Map']/parent::*</value>
      <webElementGuid>7ffe8ac7-c956-432b-9a11-047029493e77</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/ul/li[2]/a</value>
      <webElementGuid>8a88ab08-a66d-4096-a512-86ef1081a04e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@title = 'Switch to Site Map menu layout' and (text() = ' Site Map ' or . = ' Site Map ')]</value>
      <webElementGuid>47ee52c8-301e-4916-9cac-ff7d58e60bc4</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
